import React from "react";
import gpesLogo from "../../img/gpes.png";

const Footer: React.FC = () => {
  return (
    <nav className="navbar navbar-light fixed-bottom bg-light">
      <img
        src={gpesLogo}
        width="5%"
        height="5%"
        className="d-inline-block align-top ml-5"
        alt=""
      />
      <p className="text-center mb-0">
        &copy;GPES - Grupo de Gerência de Projetos em Engenharia de Software
      </p>
    </nav>
  );
};

export default Footer;
