import React, { useState, useEffect } from "react";
import "./style.css";
import {
  getApiPractices,
  createApiPractice,
} from "../../services/PraticaService";
import { getApiCategories } from "../../services/CategoriaService";
import { Categoria } from "../Categoria/Categoria";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPencilAlt } from "@fortawesome/free-solid-svg-icons";
import { faTrashAlt } from "@fortawesome/free-solid-svg-icons";

export interface Pratica {
  id?: string;
  titulo: string;
  categorias: Categoria[];
}

interface ErrorTypes {
  title?: string;
  category?: string;
  postPractice?: string;
  network?: string;
}

const Pratica: React.FC = () => {
  const [loggedIn, setLoggedIn] = useState(true);
  const [categories, setCategories] = useState<Categoria[]>([]);
  const [practiceTitle, setPracticeTitle] = useState("");
  const [practiceCategory, setPracticeCategory] = useState("");
  const [practices, setPractices] = useState<Pratica[]>([]);
  const [submitted, setSubmitted] = useState(false);
  const [success, setSuccess] = useState(false);
  const [successMsg, setSuccessMsg] = useState("");
  const [errors, setErrors] = useState<ErrorTypes>();
  const [loading, setLoading] = useState(false);
  // const [edit, setEdit] = useState(false);
  // const [deletePractice, setDeletePractice] = useState<Pratica>();

  useEffect(() => {
    if (localStorage.getItem("SECRET_KEY") === null) {
      setLoggedIn(true);
    }

    getCategories();
    getPractices();

    return () => {};
  }, []);

  async function getCategories() {
    setLoading(true);
    try {
      const response = await getApiCategories();
      setCategories(response.data);
    } catch (error) {
      setErrors({ network: "Não foi possível carregar categorias" });
    }
    setLoading(false);
  }

  async function getPractices() {
    setLoading(true);
    try {
      const response = await getApiPractices();
      setPractices(response.data);
    } catch (error) {
      setErrors({ network: "Não foi possível carregar práticas" });
    }
    setLoading(false);
  }

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setSubmitted(true);

    if (!practiceTitle || !practiceCategory) {
      setErrors({
        title: !practiceTitle ? "Preencha o titulo" : "",
        category: !practiceCategory ? "Preencha a categoria" : "",
      });
    } else {
      try {
        await createApiPractice(practiceTitle, Number(practiceCategory));
      } catch (error) {
        setErrors((errors) => ({
          ...errors,
          postPractice: "Servidor não pode ser alcançado",
        }));
      }
      setSuccess(true);
      setSuccessMsg("Sucesso");
    }
    getApiCategories();
    getApiPractices();
    setPracticeTitle("");
    setPracticeCategory("");
  };

  // const onEdite = () => {
  //   const pratica = {
  //     id: "",
  //     titulo: "",
  //     categorias: "",
  //   };
  //   window.localStorage.removeItem("editpraticaId");
  //   let praticaID = window.localStorage.setItem(
  //     "editpraticaId",
  //     pratica.id.toString()
  //   );
  //   setEdit(!edit);
  // };

  // const handleEdite = () => {};

  // const onDelete = () => {
  //   const pratica = {
  //     id: "",
  //     titulo: "",
  //     categorias: [],
  //   };
  //   window.localStorage.removeItem("removepratica");
  //   let praticaId = window.localStorage.setItem(
  //     "deletepraticaId",
  //     pratica.id.toString()
  //   );
  //   console.log(deletePratica);
  //   setDeletePratica(pratica);
  // };

  // TODO terminar esta parte
  // const handleDelete = () => {
  //   let praticaId = window.localStorage.getItem("deletepraticaId");
  //   //isso abaixo tem que retornar uma pratica
  //   let pratica = praticas.filter((prat) =>
  //     prat.id ? prat.id.toString() === praticaId : ""
  //   );
  //   setPractices(praticas.filter((prat) => prat !== pratica[0]));
  //   // deletePratica(pratica[0])

  //   setSuccess(true);
  //   setSuccessMsg(`Boa prática ${pratica[0].titulo} removida!`);
  //   setTimeout(() => setSuccess(false), 1300);
  // };

  return (
    <>
      <div className="text-center mt-5">
        <h1>Boa Prática</h1>
      </div>

      {/* Primeira parte */}
      <div className="container mt-5 mb-5 p-5 shadow">
        {/* Inicio Form */}
        {loggedIn && (
          <form onSubmit={handleSubmit}>
            <div className="form-row align-items-center justify-content-md-center">
              <div className="col-sm-4 my-1">
                <label className="sr-only">Título</label>
                <input
                  type="text"
                  className="form-control"
                  id="titulo-pratica"
                  placeholder="Título"
                  name="titulo"
                  value={practiceTitle}
                  onChange={(e) => setPracticeTitle(e.target.value)}
                />
              </div>

              <div className="col-sm-3 my-1">
                <label className="sr-only">Categoria</label>
                <select
                  className="custom-select form-control"
                  id="categoria"
                  name="categoria"
                  value={practiceCategory}
                  onChange={(e) => setPracticeCategory(e.target.value)}
                >
                  <option>Escolha uma categoria</option>

                  {categories.map((categoria) => {
                    return (
                      <option
                        key={categoria.id ? categoria.id : categoria.titulo}
                        value={categoria.id ? categoria.id : -1}
                      >
                        {categoria.titulo}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="col-auto my-1">
                <button type="submit" className="btn btn-success">
                  Salvar
                </button>
              </div>
            </div>

            {success && (
              <div
                className="alert alert-success alert-dismissible mt-2"
                role="alert"
              >
                {successMsg}
                <button
                  type="button"
                  className="close"
                  data-dismiss="alert"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            )}

            {submitted && errors && (
              <div className="error">
                <div>{errors.title}</div>
                <div>{errors.postPractice}</div>
                <div>{errors.category}</div>
              </div>
            )}
          </form>
        )}
        {/* Fim do form */}

        <table className="table table-hover mt-2 text-center">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Titulo</th>
              <th scope="col">Categoria</th>
              {loggedIn && (
                <>
                  <th scope="col">Editar</th>
                  <th scope="col">Deletar</th>
                </>
              )}
            </tr>
          </thead>
          <tbody>
            {loading && (
              <tr>
                <td colSpan={loggedIn ? 5 : 3}>Carregando</td>
              </tr>
            )}

            {errors?.network ? (
              <tr>
                <td colSpan={loggedIn ? 5 : 3}>{errors.network}</td>
              </tr>
            ) : (
              practices.map((pratica) => {
                return (
                  <tr key={pratica.id ? pratica.id : -1}>
                    <th scope="row">{pratica.id}</th>
                    <td>{pratica.titulo}</td>
                    <td>{pratica.categorias[0].titulo}</td>

                    {/* TODO onEdite e onDelete */}
                    {loggedIn && (
                      <>
                        <td>
                          <button
                            className="btn btn-transparency"
                            data-toggle="modal"
                            data-target="#editModal"
                          >
                            {/* <i className="fas fa-pencil-alt"></i> */}
                            <FontAwesomeIcon icon={faPencilAlt} />
                          </button>
                        </td>
                        <td>
                          <button
                            className="btn btn-transparency"
                            data-toggle="modal"
                            data-target="#deleteModal"
                          >
                            {/* <i className="fas fa-trash-alt"></i> */}
                            <FontAwesomeIcon icon={faTrashAlt} />
                          </button>
                        </td>
                      </>
                    )}
                  </tr>
                );
              })
            )}
          </tbody>
        </table>
      </div>
      {/* Fim Primeira parte */}

      {/* Modal Deletar */}
      <div
        className="modal fade"
        id="deleteModal"
        role="dialog"
        aria-labelledby="deleteModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="deleteModalLabel">
                Deletar prática
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              {/* TODO verificar o deletePratica pois deveria ter tipo Pratica apenas */}
              {/* Deseja realmente excluir { deletePratica.titulo } ? */}
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-dismiss="modal"
              >
                Cancelar
              </button>
              {/* TODO handleDelete */}
              <button
                type="button"
                className="btn btn-danger"
                data-dismiss="modal"
              >
                Apagar
              </button>
            </div>
          </div>
        </div>
      </div>

      {/* Modal Editar */}
      <div
        className="modal fade "
        id="editModal"
        role="dialog"
        aria-labelledby="editModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="editModalLabel">
                Editar prática
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              {/* TODO handleModalEdite */}
              <form>
                <div className="form-row align-items-center justify-content-md-center">
                  <div className="col-sm-4 my-1">
                    <label className="sr-only">Título</label>
                    <input
                      type="text"
                      className="form-control"
                      id="titulo-edit"
                      name="titulo-edit"
                      placeholder="Título"
                    />
                    {submitted && errors?.title && (
                      <div className="error position-absolute">
                        <div>Preencha o campo titulo!</div>
                      </div>
                    )}
                  </div>

                  <div className="col-sm-3 my-1">
                    <label className="sr-only">Categoria</label>
                    <select
                      className="custom-select form-control"
                      id="categoria-edit"
                      name="categoria-edit"
                    >
                      <option value="" disabled>
                        Escolha uma categoria
                      </option>
                      {categories.map((categoria) => {
                        return (
                          <option
                            key={categoria.id ? categoria.id : categoria.titulo}
                            value={categoria.titulo}
                          >
                            {categoria.titulo}
                          </option>
                        );
                      })}
                    </select>
                    {/* TODO implementar a parte de errors */}
                    {/* {submitted && errors.categoria && 
                        <div className="error position-absolute">
                          <div >Escolha uma categoria!</div>
                        </div>
                      } */}
                  </div>
                  <div className="col-auto my-1">
                    {/* TODO edite() */}
                    <button
                      type="submit"
                      className="btn btn-success"
                      data-dismiss="modal"
                    >
                      Salvar
                    </button>
                  </div>
                </div>
              </form>
            </div>

            {/* TODO revisar esta parte que estava comentada antes do port angular-react */}
            {/* <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" className="btn btn-primary" (click)="edite(editPratica)">Salvar</button>
              </div> */}
          </div>
        </div>
      </div>
    </>
  );
};

export default Pratica;
