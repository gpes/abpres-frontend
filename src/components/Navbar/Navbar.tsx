import React from "react";
import { Link } from "react-router-dom";
import "../Navbar/style.css";
import gpesBranco from "../../img/gpes-branco.png";

const Navbar = () => {
  // const [currentUser, setCurrentUser] = useState(null)

  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-green">
      <Link to="/" className="navbar-brand tx-start">
        <img
          src={gpesBranco}
          width="12%"
          height="12%"
          className="d-inline-block align-top ml-5"
          alt=""
        />
      </Link>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon"></span>
      </button>

      <div
        className="collapse navbar-collapse justify-content-end w-100"
        id="navbarSupportedContent"
      >
        <ul className="navbar-nav mr-4">
          <li className="nav-item">
            <Link to="/" className="nav-link text-light mr-2">
              Home
            </Link>
          </li>
          <li className="nav-item">
            <Link to="frameworks" className="nav-link text-light mr-2">
              Frameworks
            </Link>
          </li>
          <li className="nav-item">
            <Link to="boa-pratica" className="nav-link text-light mr-2">
              O que são boas Práticas
            </Link>
          </li>
          <li className="nav-item">
            {/* TODO mudar de volta para projeto-agil depois de achar um lugar para categoria */}
            <Link to="categoria" className="nav-link text-light mr-2">
              Projeto Ágil
            </Link>
          </li>
          <li className="nav-item">
            <Link to="login" className="nav-link text-light mr-2">
              Login
            </Link>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default Navbar;
