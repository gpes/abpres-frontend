import React, { useEffect } from "react";
// import { getCategorias } from "../../services/PraticaService";

const CreateCategoria: React.FC = () => {
  // const [categoria, setCategoria] = useState({
  //   id: "",
  //   titulo : ""
  // })
  // const [categorias, setCategorias] = useState([])

  useEffect(() => {
    async function getCats() {
      // const response = await getCategorias();
      // setCategorias(response.data)
    }

    getCats();
    return () => {};
  }, []);

  return (
    <>
      <h3 className="text-center content-title">Criar Categoria</h3>
      {/* <form className="col-lg-4 col-md-4 col-sm-4" #createForm="ngForm" (ngSubmit)="onSubmit(createForm)">
        <div className="form-group">
          <label for="titulo" className="col-form-label">Título</label>
          <input type="text" [(ngModel)]="titulo" className="form-control" id="titulo"
            name="titulo" placeholder="Digite o seu titulo" required>
          <small *ngIf="titulo.errors.required" id="NameHelp" className="form-text error">Título é obrigatório.</small>
        </div>
        <button type="submit" className="btn btn-primary">Salvar</button>
      </form>  */}
      {/* TODO implementar onSubmit */}
      <form className="col-lg-3 col-md-3 col-sm-4 mx-auto">
        <div className="form-group">
          <label className="col-form-label">Título</label>
          <input
            type="text"
            className="form-control"
            id="titulo"
            name="titulo"
            placeholder="Digite o seu titulo"
            required
          />
          {/* <small *ngIf="titulo.errors.required" id="tituloHelp" className="form-text">Preencha este campo!</small> --> */}
        </div>
        <button className="btn btn-primary" type="submit">
          Salvar
        </button>
      </form>
    </>
  );
};

export default CreateCategoria;
