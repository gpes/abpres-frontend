import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPen, faTrash } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";
import { getApiCategories } from "../../services/CategoriaService";

export interface Categoria {
  id?: string;
  titulo: string;
}

const Categoria: React.FC = () => {
  // const [logado, setLogado] = useState(true)
  // const [categoria, setCategoria] = useState({
  //   id: "",
  //   titulo : ""
  // })
  const [categorias, setCategorias] = useState<Categoria[]>([]);

  useEffect(() => {
    async function getCats() {
      const response = await getApiCategories();
      setCategorias(response.data);
    }

    getCats();
    return () => {};
  }, []);

  return (
    <>
      <div className="d-flex justify-content-center flex-column align-items-center">
        <h3 className="mt-5">Categorias</h3>
        <div className="container mt-5 p-5 col-md-3 shadow">
          {/* <a className="btn btn-success mb-3 float-right">Cadastrar</a> */}
          <Link
            to="/categorias/cadastrar"
            className="btn btn-outline-success mb-3 float-right"
          >
            Cadastrar
          </Link>
          {/* <a className="btn btn-outline-success mb-3 float-right" routerLink="/categorias/cadastrar" role="button">Cadastrar</a> */}

          <table className="table table-striped text-center">
            <thead>
              <tr>
                <th scope="col">Título</th>
                <th scope="col">Opções</th>
              </tr>
            </thead>
            <tbody>
              {categorias.map((categoria) => {
                return (
                  <tr>
                    <td>{categoria.titulo}</td>
                    <td>
                      <Link to="/categorias/update">
                        <FontAwesomeIcon
                          icon={faPen}
                          style={{ fontSize: "20px" }}
                        />
                      </Link>
                      <Link to="/categorias/delete">
                        <FontAwesomeIcon
                          icon={faTrash}
                          style={{ fontSize: "20px", marginLeft: "10px" }}
                        />
                      </Link>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
};

export default Categoria;
