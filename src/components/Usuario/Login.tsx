import React, { useEffect } from "react";

const Login: React.FC = () => {
  // const [logado, setLogado] = useState(true)

  useEffect(() => {
    return () => {};
  }, []);

  return (
    <>
      <h3 className="text-center content-title mt-2">Login</h3>
      {/* TODO implementar onSubmit */}
      <form className="col-lg-4 col-md-4 col-sm-4 mx-auto">
        <div className="form-group mt-5">
          {/* <label for="login" className="col-form-label">Login</label> */}
          <input
            type="text"
            className="form-control"
            id="login"
            name="login"
            placeholder="Digite seu login"
          />
          {/* <!-- <small *ngIf="errors != null" id="NameHelp" className="form-text error">{{errors.login}}</small> --> */}
        </div>
        <div className="form-group">
          {/* <label for="password" className="col-form-label">Senha</label> */}
          <input
            type="password"
            className="form-control"
            id="password"
            name="password"
            placeholder="Digite a senha"
          />
          {/* <!-- <small *ngIf="errors != null" id="passwordHelp" className="form-text error">{{errors.senha}}</small> --> */}
        </div>
        <button type="submit" className="btn btn-primary">
          Salvar
        </button>
      </form>
    </>
  );
};

export default Login;
