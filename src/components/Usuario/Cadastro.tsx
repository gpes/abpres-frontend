import React, { useEffect } from "react";
// import { createUsuario } from "../../services/UsuarioService";

const Cadastro: React.FC = () => {
  // const [errors, setErrors] = useState([])
  // const [nome, setNome] = useState('')
  // const [email, setEmail] = useState('')
  // const [senha, setSenha] = useState('')
  // const [confirmaSenha, setConfirmaSenha] = useState('')

  useEffect(() => {
    return () => {};
  }, []);

  //   const submit = async () => {
  //     // TODO testar esta parte abaixo e finalizar implementacao
  //     if (nome === undefined || nome === ''){
  //       // TODO implementar a parte de erros
  //       // setErrors({
  //       //   'nomeErro': 'nome',
  //       //   'descricao' : 'Preencha este campo!'
  //       // })
  //     }
  //     if (email === undefined || email === ''){
  //       // setErrors({
  //       //   'nomeErro': 'email',
  //       //   'descricao' : 'Preencha este campo!'
  //       // })
  //     } else if (email.indexOf('@') === -1){
  //       // setErrors({
  //       //   'nomeErro': 'email',
  //       //   'descricao' : 'Informe um e-mail válido.'
  //       // })
  //     }

  //     if (senha === undefined || senha === ''){
  //       // setErrors({
  //       //   'nomeErro': 'senha',
  //       //   'descricao' : 'Preencha este campo!'
  //       // })
  //     } else if (senha.split('').length < 6){
  //       // setErrors({
  //       //   'nomeErro': 'senha',
  //       //   'descricao' : 'A senha deve ter no mínimo 6 caracteres.'
  //       // })
  //     }
  //     if (confirmaSenha === undefined || confirmaSenha === ''){
  //       // setErrors({
  //       //   'nomeErro': 'confirmaSenha',
  //       //   'descricao' : 'Preencha este campo!'
  //       // })
  //     } else if (senha !== confirmaSenha){
  //       // setErrors({
  //       //   'nomeErro': 'confirmaSenha',
  //       //   'descricao' : 'Senhas não coincidem, tente novamente.'
  //       // })
  //     }

  //     if (errors.length === 0) {
  //       let usuario = {
  //         username : nome,
  //         email,

  //         senha,
  //         tipo : 'Basico'
  //       }
  //       await createUsuario(usuario)
  //   }
  // }

  return (
    <>
      {/* TODO implementar submit */}
      <h3 className="text-center content-title">Crie a sua conta</h3>
      <form className="col-lg-4 col-md-4 col-sm-4">
        <div className="form-group">
          <label className="col-form-label">Nome de Usuário</label>
          <input
            type="text"
            className="form-control"
            id="nome"
            name="nome"
            placeholder="Digite o seu nome"
          />

          {/* {errors && <small id="NameHelp" className="form-text error">{errors.nome}</small>} */}
        </div>
        <div className="form-group">
          <label className="col-form-label">Email</label>
          <input
            type="email"
            name="email"
            className="form-control"
            id="email"
            placeholder="Digite o seu e-mail"
          />

          {/* {errors && <small id="EmailHelp" className="form-text error">{errors.email}</small>} */}
        </div>
        <div className="form-group">
          <label className="col-form-label">Senha</label>
          <input
            type="password"
            className="form-control"
            id="senha"
            name="senha"
            placeholder="Digite a sua senha"
          />

          {/* {errors && <small id="passwordHelp" className="form-text error">{errors.senha}</small>} */}
        </div>
        <div className="form-group">
          <label className="col-form-label">Confirme a senha</label>
          <input
            type="password"
            name="confirmaSenha"
            className="form-control"
            id="confirmaSenha"
            placeholder="Digite novamente a senha"
          />

          {/* {errors && <small id="passwordConfirmHelp" className="form-text error">{errors.confirmaSenha}</small>} */}
        </div>
        <button type="submit" className="btn btn-primary">
          Salvar
        </button>
      </form>
    </>
  );
};

export default Cadastro;
