export interface Usuario {
  id?: number,
  username: String,
  email: String,
  senha: String,
  tipo: String
}