import React from 'react';
import './style.css';

const Home : React.FC = () => {

  if (localStorage.getItem('reloadNavbar') != null && localStorage.getItem('reloadNavbar') === 'true') {
    window.location.reload()
    localStorage.setItem('reloadNavbar', 'false');
  }   

  return (
    <section>
      <br/>
      <br/>
      <h1 className="text-center pt-5">Encontre as melhores metodologias e práticas para o seu projeto!</h1>
      <br/>
      <form className="container">
          <div className="form-group w-75 mx-auto">
            <input type="text" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Pesquisar metodologias..." />
          </div>
          <div className="text-center">
            <button type="submit" className="btn btn-outline-light">Buscar</button>
          </div>
      </form>
      <br/>
      <br/>
    </section>
  )
}

export default Home