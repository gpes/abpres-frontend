import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./components/Home/Home";
import Navbar from "./components/Navbar/Navbar";
import Footer from "./components/Footer/Footer";
import Pratica from "./components/Pratica/Pratica";
import Categoria from "./components/Categoria/Categoria";
import CreateCategoria from "./components/Categoria/CreateCategoria";
import Login from "./components/Usuario/Login";
import Cadastro from "./components/Usuario/Cadastro";

function App() {
  return (
    <div className="App">
      <Router>
        <Navbar />

        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/boa-pratica" exact component={Pratica} />
          <Route path="/categoria" exact component={Categoria} />
          <Route
            path="/categorias/cadastrar"
            exact
            component={CreateCategoria}
          />
          <Route path="/login" exact component={Login} />
          <Route path="/cadastrar" exact component={Cadastro} />

          {/* <Route path="*" component={ErrorRoute} /> */}
        </Switch>
      </Router>
      <Footer />
    </div>
  );
}

export default App;
