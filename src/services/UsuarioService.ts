import { Usuario } from "../components/Usuario/UsuarioInterface";
import api from "./api";

export async function getUsuarios() {
  return await api.get("/usuarios");
}

export async function createUsuario(usuario: Usuario) {
  return await api.post("usuarios", {
    usuario,
  });
}

export async function login(usuario: Usuario) {
  let login = usuario.email;
  let senha = usuario.senha;
  const result = await api.post("usuarios/autenticar", {
    login,
    senha,
  });
  // TODO testar esta parte
  const user = result.data;

  if (user) {
    localStorage.setItem("currentUser", JSON.stringify(user));
    // localStorage.setItem('SECRET_KEY', user.headers.get('Authorization'));
    localStorage.setItem("reloadNavbar", "true");
  }
}

export function logout() {
  localStorage.removeItem("currentUser");
  localStorage.removeItem("SECRET_KEY");
}

export default api;
