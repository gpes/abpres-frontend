import { Pratica } from "../components/Pratica/Pratica";
import api from "./api";

export async function getApiPractices() {
  return await api.get("/praticas");
}

export async function getPractice(pratica: Pratica) {
  return await api.get(`/praticas/${pratica.id}`);
}

export async function createApiPractice(prat: string, categoria: number) {
  return await api.post(`praticas/${categoria}`, {
    titulo: prat,
  });
}

export async function updatePractice(pratica: Pratica) {
  return await api.put(`praticas/${pratica.id}`, {
    headers: { Authorization: localStorage.getItem("SECRET_KEY") },
    pratica,
  });
}

export async function deletePractice(pratica: Pratica) {
  return await api.delete(`praticas/${pratica.id}`, {
    headers: { Authorization: localStorage.getItem("SECRET_KEY") },
  });
}

export default api;
