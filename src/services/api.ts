import axios from "axios";

const api = axios.create({
  baseURL: `${process.env.REACT_APP_URL_LOCALHOST}`,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
    "Acces-Control-Allow-Origin": "*",
  },
});

export default api;
