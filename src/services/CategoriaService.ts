import { Categoria } from "../components/Categoria/Categoria";
import api from "./api";

export async function getApiCategories() {
  return await api.get("/categorias");
}

export async function createApiCategories(categoria: Categoria) {
  return await api.post("categorias", {
    headers: { Authorization: localStorage.getItem("SECRET_KEY") },
    categoria,
  });
}

export default api;
